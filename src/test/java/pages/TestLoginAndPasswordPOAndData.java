package pages;

import org.testng.annotations.Test;

import static pages.pageWithSelenide.*;

public class TestLoginAndPasswordPOAndData extends baseTestClass {


    @Test(description = "Проверка ввода корректных логина и пароля", priority = 0)
    public void TestLoginAndPasswordPOAndData() {

        //Открываем страницу
        openMainPage();

        //Вводим логин и пароль
        searchFieldName.sendKeys("tomsmith");
        searchFieldPassword.sendKeys("SuperSecretPassword!");

        //Запускаем поиск
        pressEnterForElement(searchFieldPassword);
        checkElementVisible(searchCorrectResults);
    }

    @Test(description = "Проверка ввода некорректного логина", priority = 1)
    public void testCheckIncorrectLoginPOAndData() {

        //Открываем страницу
        openMainPage();

        //Вводим логин и пароль
        searchFieldName.sendKeys("tomsmith1");
        searchFieldPassword.sendKeys("SuperSecretPassword!");

        //Запускаем поиск
        pressEnterForElement(searchFieldPassword);
        checkElementVisible(searchInCorrectResultsUsername);
    }

    @Test(description = "Проверка ввода некорректного пароля", priority = 2)
    public void testCheckIncorrectPasswordPOAndData() {

        //Открываем страницу
        openMainPage();

        //Вводим логин и пароль
        searchFieldName.sendKeys("tomsmith");
        searchFieldPassword.sendKeys("SuperSecretPassword!1");

        //Запускаем поиск
        pressEnterForElement(searchFieldPassword);
        checkElementVisible(searchInCorrectResultsPassword);
    }

}